# Catalan translation of Notepad
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as the Notepad package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#
msgid ""
msgstr ""
"Project-Id-Version: Notepad\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2010-12-17 01:09+0100\n"
"Last-Translator: Jordi Sayol <g.sayol@yahoo.es>\n"
"Language-Team: \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Catalan\n"

#: FNotepad.form:122
msgid "&?"
msgstr "-"

#: FNotepad.form:125
msgid "&About..."
msgstr "&Quant a..."

#: FAbout.form:10
msgid "About..."
msgstr "Quant a..."

#: .project:1
msgid "A little text editor"
msgstr "Un petit editor de text"

#: FNotepad.class:118
msgid "All files"
msgstr "Tots els fitxers"

#: FNotepad.class:66
msgid "Cancel"
msgstr "Canceŀla"

#: FNotepad.class:118
msgid "C/C++ files"
msgstr "Fitxers C/C++"

#: FNotepad.form:111
msgid "Choose &Font..."
msgstr "&Tria el tipus de lletra..."

#: FNotepad.form:42
msgid "Close"
msgstr "&Tanca"

#: FNotepad.form:75
msgid "&Copy"
msgstr "&Copia"

#: FNotepad.form:81
msgid "C&ut"
msgstr "&Retalla"

#: FNotepad.class:118
msgid "Desktop files"
msgstr "Fitxers d'escriptori"

#: FNotepad.form:72
msgid "&Edit"
msgstr "&Edita"

#: FNotepad.form:33
msgid "&File"
msgstr "&Fitxer"

#: FNotepad.form:28
msgid "Little notepad"
msgstr "Petit bloc de notes"

#: FNotepad.class:30
msgid "(New document)"
msgstr "(Document nou)"

#: FNotepad.class:66
msgid "\n\nFile has been modified. Do you want to save it ?"
msgstr "\n\nS'ha modificat el fitxer. El voleu desar?"

#: FNotepad.class:66
msgid "No"
msgstr "-"

#: FNotepad.class:97
msgid "\nUnable to load file.\n"
msgstr "\nNo es pot carregar el fitxer.\n"

#: FAbout.form:21
msgid "OK"
msgstr "D'acord"

#: FNotepad.form:36
msgid "&Open..."
msgstr "&Obre..."

#: FNotepad.form:87
msgid "&Paste"
msgstr "&Enganxa"

#: FNotepad.form:65
msgid "&Quit"
msgstr "&Surt"

#: FNotepad.form:102
msgid "&Redo"
msgstr "Re&fés"

#: FNotepad.form:51
msgid "&Save"
msgstr "&Desa"

#: FNotepad.form:57
msgid "S&ave As..."
msgstr "D&esa com..."

#: FNotepad.class:118
msgid "Text files"
msgstr "Fitxers de text"

#: FAbout.form:28
msgid "This is a little notepad sample program."
msgstr "Aquest és un petit programa exemple de bloc de notes."

#: FNotepad.form:133
msgid "txtNotepad"
msgstr "-"

#: FNotepad.form:96
msgid "&Undo"
msgstr "&Desfés"

#: FNotepad.form:115
msgid "&Wrap text"
msgstr "&Ajusta el text"

#: FNotepad.class:66
msgid "Yes"
msgstr "Sí"

