��          |      �             !     *  P   1  �   �          0     <  6   E  ,   |     �     �     �     �  	   �  ;   �  �        �     �     �  0   �  $        ?     P               
             	                               &Discard &Embed Click on the <b>Discard</b> button to free the application window from its jail. Click on the <b>Embed</b> button to search the window whose title 
is specified in the left field, and to embed it in the 
blue rectangle below. Desktop application embedder Embed error Embedder Enter there the title of the window you want to embed. Several windows found. I take the first one! Window not found! Window title Project-Id-Version: PACKAGE VERSION
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 &Zrušit Za&pustit Klikněte na <b>Zrušit</b> a okno aplikace bude uvolněno. Klikněte na tlačítko <b>Zapustit</b> pro vyhledávání okna, jehož název
je uvedené v levém poli, a zapuštěno bude do
modrého obdélníku níže. Aplikace zapuštěna Chyba zapuštění Zapoštěč Zde zadejte název okna, které chcete zapustit. Nalezeno několik obek. Beru první! Okno nenalezeno! Titulek okna 